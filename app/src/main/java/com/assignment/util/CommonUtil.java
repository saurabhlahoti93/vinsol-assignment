package com.assignment.util;


import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.view.inputmethod.InputMethodManager;

import com.assignment.apiResponse.Schedule;
import com.assignment.interfaces.CustomPopupInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;

public class CommonUtil implements Constants {
    public static Context context;
    public static int bookingView = 0;
    private static File file;
    private File tempFile, imageFile;

    public boolean checkInternet() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
            if (ipAddr.equals("")) {
                return false;
            } else {
                return true;
            }

        } catch (Exception e) {
            return false;
        }
    }

    public boolean isInternetAvailable(Context context) {
        try {//checking network status
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Exception e) {
            return true;
        }
    }

    public void hideKeyBoard(final Activity mContext) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (mContext.getCurrentFocus() != null)
                    inputManager.hideSoftInputFromWindow(mContext.getCurrentFocus().getWindowToken(), 0);
            }
        }, 200);

    }


    public Date getDate(String timeString) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_TIME);
        Date date = format.parse(timeString);
        return date;
    }

    public void sortSchedule(List<Schedule> listItems) {
        Collections.sort(listItems, new Comparator<Schedule>() {
            // @Override
            public int compare(Schedule c1, Schedule c2) {
                return (convertTimeToDate(c1.startTime).compareTo(convertTimeToDate(c2.startTime)));
            }
        });
    }


    Date convertTimeToDate(String time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        try {
            return simpleDateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public String getTimeFormatted(String time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(DATE_FORMAT_TIME);
        String s = "";
        try {
            Date date = simpleDateFormat.parse(time);
            s = simpleDateFormat2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return s;
    }

    public void handleRetrofitFailure(Context context, RetrofitError error) {
        CustomPopup customPopup = new CustomPopup();
        try {
            if (error.getResponse() != null) {
                String json = new String(((TypedByteArray) error.getResponse()
                        .getBody()).getBytes());
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    if (jsonObject.get("statusCode") != null) {
                        if (jsonObject.get("statusCode").toString().compareTo("403") == 0) {
                        } else {
                            customPopup.createToast(jsonObject.get("message").toString(), context);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                customPopup.createPopup(EMPTY_RESPONSE, "Ok", "", context, true, false, new CustomPopupInterface() {
                    @Override
                    public void positiveClick() {

                    }

                    @Override
                    public void negativeClick() {

                    }

                    @Override
                    public void neutralClick() {

                    }
                });
            }
        } catch (RetrofitError e) {
            e.printStackTrace();
        }
    }


}
