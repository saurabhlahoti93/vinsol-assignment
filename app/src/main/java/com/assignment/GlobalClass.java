package com.assignment;


import android.app.Application;
import android.content.Context;

import com.assignment.util.WebServices;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class GlobalClass extends Application{
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    final String DOMAIN = "http://fathomless-shelf-5846.herokuapp.com/api";

    public static WebServices webServices;
    public WebServices getInterface() {
        if(webServices == null){
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setReadTimeout(15, TimeUnit.SECONDS);
            okHttpClient.setConnectTimeout(15, TimeUnit.SECONDS);
            RestAdapter.Builder builder = new RestAdapter.Builder()
                    .setEndpoint(DOMAIN)
                    .setClient(new OkClient(okHttpClient))
                    .setLogLevel(RestAdapter.LogLevel.FULL);
            RestAdapter adapter = builder.build();
            webServices = adapter.create(WebServices.class);
        }
        return webServices;
    }

}
