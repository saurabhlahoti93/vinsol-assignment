package com.assignment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.assignment.R;
import com.assignment.apiResponse.Schedule;
import com.assignment.util.CommonUtil;

import java.util.List;

/**
 * Created by Saurabh on 30-06-2016.
 */
public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.MyHolder> {
    List<Schedule> scheduleList;
    LayoutInflater layoutInflater;
    CommonUtil commonUtil;
    public ScheduleAdapter(Context context, List<Schedule> scheduleList) {
        this.scheduleList = scheduleList;
        layoutInflater = LayoutInflater.from(context);
        commonUtil = new CommonUtil();
    }

    public void setScheduleList(List<Schedule> scheduleList) {
        this.scheduleList = scheduleList;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_schedule, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        Schedule schedule = scheduleList.get(position);
        holder.tvSchDesc.setText(schedule.description);
        holder.tvSchStart.setText(commonUtil.getTimeFormatted(schedule.startTime));
        holder.tvSchEnd.setText(commonUtil.getTimeFormatted(schedule.endTime));
        if (holder.tvSchParticipant != null) {
            String participants = "";
            int i;
            for (i = 0; i < schedule.participants.size() - 1; i++) {
                participants += schedule.participants.get(i) + ", ";
            }
            participants += schedule.participants.get(i);
            holder.tvSchParticipant.setText(participants);
        }
    }

    @Override
    public int getItemCount() {
        return scheduleList.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        public TextView tvSchDesc, tvSchParticipant, tvSchStart, tvSchEnd;

        public MyHolder(View itemView) {
            super(itemView);
            tvSchDesc = (TextView) itemView.findViewById(R.id.tvScheduleDesc);
            tvSchStart = (TextView) itemView.findViewById(R.id.tvScheduleStart);
            tvSchEnd = (TextView) itemView.findViewById(R.id.tvScheduleEnd);
            try {
                tvSchParticipant = (TextView) itemView.findViewById(R.id.tvScheduleParticipants);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
