package com.assignment.entities;

import java.util.ArrayList;

/**
 * Created by Saurabh on 01-07-2016.
 */
public class WorkDays {
    ArrayList<Boolean>workDays;

    public ArrayList<Boolean> getWorkDays() {
        return workDays;
    }

    public void setWorkDays(ArrayList<Boolean> workDays) {
        this.workDays = workDays;
    }
}
