package com.assignment.util;

public interface Constants {

    String MY_SHARED_PREFERENCES = "application_data";


    String KEY_WORK_TIME = "workTimeData";
    String KEY_WORK_DAYS = "workDaysData";
    String KEY_TIME_SLOT = "slotData";
    String KEY_SCHEDULE = "scheduleData";


    String DATE_FORMAT_PORTRAIT = "dd-MM-yyyy";
    String DATE_FORMAT_TIME = "hh:mm a";
    String SERVER_TIME = "HH:mm";
    String DATE_FORMAT_LANDSCAPE = "EEE, dd-MM-yyyy";
    String DATE_FORMAT_REQUEST = "dd/MM/yyyy";

    String DEFAULT_START_TIME = "09:00 AM";
    String DEFAULT_END_TIME = "06:00 PM";

    String INTERNET_CONNECTIVITY = "No internet connection. Please try again.";
    String EMPTY_RESPONSE = "Some error occurred. Please check your internet connectivity and try again later.";

}
