package com.assignment.entities;

/**
 * Created by Saurabh on 01-07-2016.
 */
public class WorkTime {
    String workStartTime;
    String workEndTime;

    public String getWorkStartTime() {
        return workStartTime;
    }

    public void setWorkStartTime(String workStartTime) {
        this.workStartTime = workStartTime;
    }

    public String getWorkEndTime() {
        return workEndTime;
    }

    public void setWorkEndTime(String workEndTime) {
        this.workEndTime = workEndTime;
    }
}
