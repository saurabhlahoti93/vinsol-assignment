package com.assignment.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.assignment.R;
import com.assignment.interfaces.CustomPopupInterface;


public class CustomPopup {
    Dialog dialog;

    public void createPopup(String message, String positive, String negative,
                            Context context, Boolean cancelable, boolean isPlow, final CustomPopupInterface customPopupInterface) {
        if (isPlow) {
            try {
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                dialog.setContentView(R.layout.popup_generic);
                dialog.setCancelable(cancelable);
                TextView messageTxt = (TextView) dialog.findViewById(R.id.popupMsg);
                Button positiveBtn = (Button) dialog.findViewById(R.id.positive);
                Button negativeBtn = (Button) dialog.findViewById(R.id.negative);

                if (negative.length() == 0) {
                    negativeBtn.setVisibility(View.GONE);
                }
                if (positive.length() == 0) {
                    positiveBtn.setVisibility(View.GONE);
                }
                messageTxt.setText(message);
                positiveBtn.setText(positive);
                negativeBtn.setText(negative);
                dialog.show();

                positiveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customPopupInterface.positiveClick();
                        dialog.dismiss();
                    }
                });
                negativeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customPopupInterface.negativeClick();
                        dialog.dismiss();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                dialog.setContentView(R.layout.popup_generic);
                dialog.setCancelable(cancelable);

                TextView messageTxt = (TextView) dialog.findViewById(R.id.popupMsg);
                Button positiveBtn = (Button) dialog.findViewById(R.id.positive);
                Button negativeBtn = (Button) dialog.findViewById(R.id.negative);

                if (negative.length() == 0) {
                    negativeBtn.setVisibility(View.GONE);
                }
                if (positive.length() == 0) {
                    positiveBtn.setVisibility(View.GONE);
                }
                messageTxt.setText(message);
                positiveBtn.setText(positive);
                negativeBtn.setText(negative);
                dialog.show();

                positiveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customPopupInterface.positiveClick();
                        dialog.dismiss();
                    }
                });
                negativeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customPopupInterface.negativeClick();
                        dialog.dismiss();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void createToast(String message, Context context) {
        try {
            Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 80);
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Dialog createProgressDialog(Context context, Boolean cancel) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(cancel);
        dialog.setContentView(R.layout.progress_dialog);
//        ((ProgressBar)dialog.findViewById(R.id.progressBar)).getIndeterminateDrawable().setColorFilter(new LightingColorFilter(0x99000000,R.color.colorAccent));
        ((ProgressBar) dialog.findViewById(R.id.progressBar)).getIndeterminateDrawable().
                setColorFilter(ContextCompat.getColor(context, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
        return dialog;
    }


}
