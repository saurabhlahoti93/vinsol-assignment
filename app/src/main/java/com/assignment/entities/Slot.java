package com.assignment.entities;

/**
 * Created by Saurabh on 01-07-2016.
 */
public class Slot {
    String startTime;
    String endTime;

    public Slot(String startTime, String endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return startTime + " - " + endTime;
    }
}
