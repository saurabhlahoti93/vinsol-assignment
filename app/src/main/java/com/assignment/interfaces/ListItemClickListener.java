package com.assignment.interfaces;

public interface ListItemClickListener {
    void positiveListItemClicked(int position);
    void negativeListItemClicked(int position);
}

