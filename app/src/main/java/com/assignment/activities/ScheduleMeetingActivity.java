package com.assignment.activities;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

import com.assignment.GlobalClass;
import com.assignment.R;
import com.assignment.apiResponse.Schedule;
import com.assignment.entities.ScheduleData;
import com.assignment.entities.Slot;
import com.assignment.entities.WorkDays;
import com.assignment.entities.WorkTime;
import com.assignment.util.CommonUtil;
import com.assignment.util.Constants;
import com.assignment.util.CustomPopup;
import com.assignment.util.SharedPrefUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ScheduleMeetingActivity extends AppCompatActivity implements Constants,
        OnClickListener, OnTimeSetListener, OnTimeChangedListener, RadioGroup.OnCheckedChangeListener {

    Toolbar toolbar;
    ScheduleData scheduleData;
    TextView tvDate, tvStartTime, tvEndTime;
    EditText etDesc;

    WorkDays workDays;
    WorkTime workTime;
    Integer slotInterval;

    SharedPrefUtil sharedPrefUtil;
    CommonUtil commonUtil;
    CustomPopup customPopup;
    Calendar calendar;
    Calendar workStartTime;
    Calendar workEndTime;
    Calendar meetingStartTime;
    Calendar meetingEndTime;
    SimpleDateFormat requestDateFormat = new SimpleDateFormat(DATE_FORMAT_REQUEST);
    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_LANDSCAPE);
    SimpleDateFormat workTimeFormat = new SimpleDateFormat(DATE_FORMAT_TIME);
    SimpleDateFormat serverTimeFormat = new SimpleDateFormat(SERVER_TIME);

    boolean isStartTime = false, isSlotSelected = false;
    float duration;
    Boolean[] slots = new Boolean[49];

    Dialog dialog, progress;
    RadioGroup radioGroup;
    RadioButton radioButton;
    Button btnCheckNext, btnSchedule;
    LinearLayout layoutOr;
    ScrollView scrollView;
    TextView emptyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_meeting);

        initData();
        initViews();

    }

    void initData() {
        sharedPrefUtil = new SharedPrefUtil(this);
        commonUtil = new CommonUtil();
        customPopup = new CustomPopup();
        progress = customPopup.createProgressDialog(this, true);
        scheduleData = (ScheduleData) getIntent().getSerializableExtra(KEY_SCHEDULE);
        workDays = sharedPrefUtil.getWorkDayDataFromPref();
        workTime = sharedPrefUtil.getWorkTimeDataFromPref();
        slotInterval = sharedPrefUtil.getSlotDataFromPref();
        calendar = Calendar.getInstance();
        workStartTime = Calendar.getInstance();
        workEndTime = Calendar.getInstance();
        Calendar calS = Calendar.getInstance();
        Calendar calE = Calendar.getInstance();
        meetingStartTime = Calendar.getInstance();
        meetingEndTime = Calendar.getInstance();
        String startTime = workTime.getWorkStartTime();
        String endTime = workTime.getWorkEndTime();
        try {
            calS.setTime(workTimeFormat.parse(startTime));
            calE.setTime(workTimeFormat.parse(endTime));
            calendar.setTime(requestDateFormat.parse(scheduleData.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        workStartTime.set(Calendar.HOUR_OF_DAY, calS.get(Calendar.HOUR_OF_DAY));
        workEndTime.set(Calendar.HOUR_OF_DAY, calE.get(Calendar.HOUR_OF_DAY));
        workStartTime.set(Calendar.MINUTE, calS.get(Calendar.MINUTE));
        workEndTime.set(Calendar.MINUTE, calE.get(Calendar.MINUTE));
        meetingStartTime.setTimeInMillis(workStartTime.getTimeInMillis());
        meetingEndTime.setTimeInMillis(workEndTime.getTimeInMillis());


        dialog = new Dialog(this);
        dialog.setContentView(R.layout.popup_slots);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

    }

    void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_meeting));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvDate = (TextView) findViewById(R.id.tvDate);
        tvDate.requestFocus();
        tvStartTime = (TextView) findViewById(R.id.tvStartTime);
        tvEndTime = (TextView) findViewById(R.id.tvEndTime);
        etDesc = (EditText) findViewById(R.id.etDesc);

        tvDate.setText(dateFormat.format(calendar.getTime()));
        radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);
        dialog.findViewById(R.id.btnBook).setOnClickListener(this);
        btnCheckNext = (Button) dialog.findViewById(R.id.btnCheckNext);
        btnSchedule = (Button) dialog.findViewById(R.id.btnBook);
        layoutOr = (LinearLayout) dialog.findViewById(R.id.layoutOR);
        scrollView = (ScrollView) dialog.findViewById(R.id.scrollView);
        emptyText = (TextView) dialog.findViewById(R.id.emptyText);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvStartTime: {
                isStartTime = true;
                TimePickerDialog timePickerDialog = new TimePickerDialog(this, this, meetingStartTime.get(Calendar.HOUR_OF_DAY),
                        meetingStartTime.get(Calendar.MINUTE), false);
                timePickerDialog.show();
            }
            break;

            case R.id.tvEndTime: {
                isStartTime = false;
                TimePickerDialog timePickerDialog = new TimePickerDialog(this, this, meetingEndTime.get(Calendar.HOUR_OF_DAY),
                        meetingEndTime.get(Calendar.MINUTE), false);
                timePickerDialog.show();
            }

            break;

            case R.id.btnSubmit:
                if (tvStartTime.getText().length() != 0 && tvEndTime.getText().length() != 0) {
                    markSlots(scheduleData.getScheduleList());
                    if (isSlotAvailable()) {
                        customPopup.createToast("Meeting scheduled successfully", this);
                        finish();
                    } else {
                        showSlotDialog(false);
                    }
                } else {
                    customPopup.createToast("Specify start & end time for meeting", this);
                }
                break;

            case R.id.btnBook:
                if (isSlotSelected) {
                    dialog.dismiss();
                    customPopup.createToast("Meeting scheduled successfully", this);
                    finish();
                }
                break;

            case R.id.btnCheckNext:
                Calendar temp = Calendar.getInstance();
                temp.setTime(calendar.getTime());
                int dayOfWeek = temp.get(Calendar.DAY_OF_WEEK) - 1;
                int nextWorkDay = 0;
                boolean workDayFound = false;
                for (int j = dayOfWeek + 1; j < 7; j++) {
                    if (workDays.getWorkDays().get(j)) {
                        workDayFound = true;
                        break;
                    }
                    nextWorkDay++;
                }
                if (!workDayFound) {
                    for (int j = 0; j <= dayOfWeek; j++) {
                        if (workDays.getWorkDays().get(j)) {
                            break;
                        }
                        nextWorkDay++;
                    }
                }
                temp.add(Calendar.DATE, nextWorkDay + 1);
                String date = requestDateFormat.format(temp.getTime());
                getScheduleData(date);
                break;
        }
    }

    @Override
    public void onTimeChanged(TimePicker timePicker, int hour, int min) {
        if (slotInterval == 0) {
            if (min > 30) {
                hour++;
            }
            min = 0;
        } else {
            if (min >= 0 && min <= 15) {
                min = 0;
            } else if (min > 15 && min <= 30) {
                min = 30;
            } else if (min > 30 && min <= 45) {
                min = 30;
            } else {
                min = 0;
                hour++;
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            timePicker.setMinute(min);
            timePicker.setHour(hour);
        }
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
        if (slotInterval == 0) {
            if (minute > 30) {
                hour++;
            }
            minute = 0;
        } else {
            if (minute >= 0 && minute <= 15) {
                minute = 0;
            } else if (minute > 15 && minute <= 30) {
                minute = 30;
            } else if (minute > 30 && minute <= 45) {
                minute = 30;
            } else {
                minute = 0;
                hour++;
            }
        }
        Calendar temp = Calendar.getInstance();
        temp.set(Calendar.HOUR_OF_DAY, hour);
        temp.set(Calendar.MINUTE, minute);
        temp.set(Calendar.MILLISECOND, 0);
        if (isStartTime) {
            if (temp.before(workStartTime) || temp.after(workEndTime)) {
                customPopup.createToast("Meeting start time should be during work hours", this);
                return;
            } else if (temp.after(meetingEndTime)) {
                customPopup.createToast("Start time should be before end time", this);
                return;
            } else {
                meetingStartTime.setTimeInMillis(temp.getTimeInMillis());
            }
            tvStartTime.setText(workTimeFormat.format(meetingStartTime.getTime()));
        } else {
            if (temp.before(workStartTime) || temp.after(workEndTime)) {
                customPopup.createToast("Meeting end time should be during work hours", this);
                return;
            } else if (temp.before(meetingStartTime)) {
                customPopup.createToast("End time should be after start time", this);
                return;
            } else {
                meetingEndTime.setTimeInMillis(temp.getTimeInMillis());
            }
            tvEndTime.setText(workTimeFormat.format(meetingEndTime.getTime()));
        }

        if (tvStartTime.length() != 0 && tvEndTime.length() != 0) {
            duration = Float.valueOf(String.format("%.1f", new Float(meetingEndTime.getTimeInMillis() - meetingStartTime.getTimeInMillis()) / (1000 * 60 * 60)));
        }
    }

    void markSlots(List<Schedule> schedules) {
        Arrays.fill(slots, Boolean.FALSE);
        for (Schedule schedule : schedules) {
            Calendar startTime = Calendar.getInstance();
            Calendar endTime = Calendar.getInstance();
            try {
                startTime.setTime(serverTimeFormat.parse(schedule.startTime));
                endTime.setTime(serverTimeFormat.parse(schedule.endTime));
                float start = startTime.get(Calendar.HOUR_OF_DAY) + (startTime.get(Calendar.MINUTE) / 60.0f);
                float end = endTime.get(Calendar.HOUR_OF_DAY) + (endTime.get(Calendar.MINUTE) / 60.0f);
                float duration = end - start;
                for (float i = 0; i < duration; i = i + 0.5f) {
                    slots[(int) ((start + i) * 2)] = true;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    boolean isSlotAvailable() {
        Float start = meetingStartTime.get(Calendar.HOUR_OF_DAY) + (meetingStartTime.get(Calendar.MINUTE) / 60.0f);

        boolean isSlotAvailable = false;
        for (Float j = start; j <= start + duration; j = j + 0.5f) {
            if (!slots[Float.valueOf(j * 2.0f).intValue()]) {
                isSlotAvailable = true;
            } else {
                isSlotAvailable = false;
                break;
            }
        }
        return isSlotAvailable;
    }

    ArrayList<String> getAvailableSlots() {
        ArrayList<String> slotsText = new ArrayList<>();
        Float start = workStartTime.get(Calendar.HOUR_OF_DAY) + (workStartTime.get(Calendar.MINUTE) / 60.0f);
        Float end = workEndTime.get(Calendar.HOUR_OF_DAY) + (workEndTime.get(Calendar.MINUTE) / 60.0f);
        for (Float i = start; i <= (end - duration); i = i + 0.5f) {
            boolean isSlotAvailable = false;
            String startTime = getTimeStringFromFloat(i);
            String endTime = getTimeStringFromFloat(i + duration);
            Slot slot = new Slot(startTime, endTime);
            for (Float j = i; j <= i + duration; j = j + 0.5f) {
                if (!slots[Float.valueOf(j * 2.0f).intValue()]) {
                    isSlotAvailable = true;
                } else {
                    isSlotAvailable = false;
                    break;
                }
            }
            if (isSlotAvailable) {
                slotsText.add(slot.toString());
            }
        }
        return slotsText;
    }

    String getTimeStringFromFloat(Float val) {
        String time = "";
        boolean isAM = false;
        if (val.intValue() < 12) {
            isAM = true;
        } else {
            isAM = false;
        }
        time += val.intValue() + ":";
        if (val - val.intValue() > 0) {
            time += ((int) ((val - val.intValue()) * 60)) + " ";
        } else {
            time += "00 ";
        }
        if (isAM) {
            time += "AM";
        } else {
            time += "PM";
        }
        return time;
    }

    void getScheduleData(final String date) {
        dialog.dismiss();
        progress.show();

        if (!commonUtil.isInternetAvailable(this)) {
            customPopup.createToast(INTERNET_CONNECTIVITY, this);
        } else {
            ((GlobalClass) getApplicationContext()).getInterface().getSchedule(date, new Callback<List<Schedule>>() {
                @Override
                public void success(List<Schedule> schedules, Response response) {
                    markSlots(schedules);
                    progress.dismiss();
                    showSlotDialog(true);
                }

                @Override
                public void failure(RetrofitError error) {
                    progress.dismiss();
                    commonUtil.handleRetrofitFailure(ScheduleMeetingActivity.this, error);
                }
            });
        }
    }

    void showSlotDialog(boolean forNextDay) {
        ArrayList<String> availableSlots = getAvailableSlots();
        if (availableSlots.size() == 0) {
            scrollView.setVisibility(View.GONE);
            btnSchedule.setVisibility(View.GONE);
            emptyText.setVisibility(View.VISIBLE);
        } else {
            btnSchedule.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.VISIBLE);
            emptyText.setVisibility(View.GONE);
        }
        if (availableSlots.size() <= 3) {
            btnCheckNext.setVisibility(View.VISIBLE);
            layoutOr.setVisibility(View.VISIBLE);
            btnCheckNext.setOnClickListener(this);
        } else {
            btnCheckNext.setVisibility(View.GONE);
            layoutOr.setVisibility(View.GONE);
            btnCheckNext.setOnClickListener(null);
        }
        if (forNextDay) {
            btnCheckNext.setVisibility(View.GONE);
            layoutOr.setVisibility(View.GONE);
            btnCheckNext.setOnClickListener(null);
        }
        radioGroup.removeAllViews();
        for (String s : availableSlots) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText("   " + s);
            RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            rprms.setMargins(5,5,5,5);
            radioGroup.addView(radioButton, rprms);
            radioGroup.setOnCheckedChangeListener(this);
        }
        dialog.show();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        isSlotSelected = true;
    }
}