package com.assignment.entities;

import com.assignment.apiResponse.Schedule;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Saurabh on 01-07-2016.
 */
public class ScheduleData implements Serializable{
    private String date;
    private List<Schedule>scheduleList;

    public void setDate(String date) {
        this.date = date;
    }

    public void setScheduleList(List<Schedule> scheduleList) {
        this.scheduleList = scheduleList;
    }

    public String getDate() {
        return date;
    }

    public List<Schedule> getScheduleList() {
        return scheduleList;
    }
}
