package com.assignment.interfaces;


public interface CustomPopupInterface {
    void positiveClick();
    void negativeClick();
    void neutralClick();
}
