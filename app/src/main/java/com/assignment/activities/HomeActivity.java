package com.assignment.activities;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.assignment.GlobalClass;
import com.assignment.R;
import com.assignment.adapters.ScheduleAdapter;
import com.assignment.apiResponse.Schedule;
import com.assignment.entities.ScheduleData;
import com.assignment.entities.WorkDays;
import com.assignment.entities.WorkTime;
import com.assignment.util.CommonUtil;
import com.assignment.util.Constants;
import com.assignment.util.CustomPopup;
import com.assignment.util.SharedPrefUtil;
import com.assignment.util.WebServices;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class HomeActivity extends AppCompatActivity implements Constants,
        OnClickListener, OnRefreshListener, OnDateSetListener, OnTimeSetListener,
        OnCheckedChangeListener {

    WebServices webServices;
    RecyclerView rvSchedule;
    ScheduleAdapter scheduleAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    CommonUtil commonUtil;
    CustomPopup customPopup;
    Toolbar toolbar;
    Calendar calendar;
    TextView tvDate;
    SimpleDateFormat requestDateFormat = new SimpleDateFormat(DATE_FORMAT_REQUEST);
    SimpleDateFormat scheduleDateFormat;
    SimpleDateFormat workTimeFormat = new SimpleDateFormat(DATE_FORMAT_TIME);
    List<Schedule> scheduleList = new ArrayList<>();
    View dummyView;

    SharedPrefUtil sharedPrefUtil;
    ScheduleData scheduleData;
    WorkDays workDays;
    WorkTime workTime;
    Integer slotTime;
    boolean ifStartTime = true;
    LayoutInflater layoutInflater;
    String[] workdaysArray;
    Dialog dialog;
    TextView tvTitle;
    LinearLayout containerLayout;
    TextView tvStart, tvEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initClasses();
        initViews();
        initData();

    }

    void initClasses() {
        webServices = ((GlobalClass) getApplicationContext()).getInterface();
        commonUtil = new CommonUtil();
        customPopup = new CustomPopup();
        sharedPrefUtil = new SharedPrefUtil(this);
        scheduleAdapter = new ScheduleAdapter(HomeActivity.this, scheduleList);
        calendar = Calendar.getInstance();
        workdaysArray = getResources().getStringArray(R.array.work_days);
        layoutInflater = LayoutInflater.from(this);
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.popup_setting);
        dialog.setCancelable(false);
    }

    void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.color_1),
                ContextCompat.getColor(this, R.color.color_2),
                ContextCompat.getColor(this, R.color.color_3),
                ContextCompat.getColor(this, R.color.color_4),
                ContextCompat.getColor(this, R.color.color_5));
        swipeRefreshLayout.setOnRefreshListener(this);
        rvSchedule = (RecyclerView) findViewById(R.id.rvSchedule);
        rvSchedule.setLayoutManager(new LinearLayoutManager(this));
        rvSchedule.setAdapter(scheduleAdapter);
        tvDate = (TextView) findViewById(R.id.tvDate);

        try {
            dummyView = findViewById(R.id.dummy);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (dummyView == null) {
            scheduleDateFormat = new SimpleDateFormat(DATE_FORMAT_PORTRAIT);
        } else {
            scheduleDateFormat = new SimpleDateFormat(DATE_FORMAT_LANDSCAPE);
        }
        tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        containerLayout = (LinearLayout) dialog.findViewById(R.id.containerLayout);
        dialog.findViewById(R.id.btnClosePopup).setOnClickListener(this);
    }

    void initData() {
        scheduleData = sharedPrefUtil.getScheduleDataFromPref();
        if (scheduleData == null) {
            scheduleData = new ScheduleData();
            getScheduleData();
        } else {
            try {
                calendar.setTime(requestDateFormat.parse(scheduleData.getDate()));
                tvDate.setText(scheduleDateFormat.format(calendar.getTime()));
                scheduleAdapter.setScheduleList(scheduleData.getScheduleList());
                scheduleAdapter.notifyDataSetChanged();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        workDays = sharedPrefUtil.getWorkDayDataFromPref();
        if (workDays == null) {
            workDays = new WorkDays();
            ArrayList<Boolean> days = new ArrayList<>();
            for (int i = 0; i < 7; i++) {
                days.add(true);
            }
            workDays.setWorkDays(days);
            sharedPrefUtil.addWorkDayDataToPref(workDays);
        }

        workTime = sharedPrefUtil.getWorkTimeDataFromPref();
        if (workTime == null) {
            workTime = new WorkTime();
            workTime.setWorkStartTime(DEFAULT_START_TIME);
            workTime.setWorkEndTime(DEFAULT_END_TIME);
            sharedPrefUtil.addWorkTimeDataToPref(workTime);
        }

        slotTime = sharedPrefUtil.getSlotDataFromPref();

    }

    void getScheduleData() {
        swipeRefreshLayout.setRefreshing(true);
        final String date = requestDateFormat.format(calendar.getTime());
        if (!commonUtil.isInternetAvailable(this)) {
            swipeRefreshLayout.setRefreshing(false);
            customPopup.createToast(INTERNET_CONNECTIVITY, this);
        } else {
            webServices.getSchedule(date, new Callback<List<Schedule>>() {
                @Override
                public void success(List<Schedule> schedules, Response response) {
                    tvDate.setText(scheduleDateFormat.format(calendar.getTime()));
                    swipeRefreshLayout.setRefreshing(false);
                    commonUtil.sortSchedule(schedules);
                    scheduleAdapter.setScheduleList(schedules);
                    scheduleAdapter.notifyDataSetChanged();
                    scheduleData.setDate(date);
                    scheduleData.setScheduleList(schedules);
                    sharedPrefUtil.addScheduleDataToPref(scheduleData);
                }

                @Override
                public void failure(RetrofitError error) {
                    swipeRefreshLayout.setRefreshing(false);
                    commonUtil.handleRetrofitFailure(HomeActivity.this, error);
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvDate:
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, this,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                datePickerDialog.show();
                break;

            case R.id.tvPrev: {
                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
                int prevWorkDay = 0;
                boolean workDayFound = false;
                for (int j = dayOfWeek - 1; j >= 0; j--) {
                    if (workDays.getWorkDays().get(j)) {
                        workDayFound = true;
                        break;
                    }
                    prevWorkDay++;
                }
                if (!workDayFound) {
                    for (int j = 6; j >= dayOfWeek; j--) {
                        if (workDays.getWorkDays().get(j)) {
                            break;
                        }
                        prevWorkDay++;
                    }
                }
                calendar.add(Calendar.DATE, -(prevWorkDay + 1));
                getScheduleData();
            }
            break;

            case R.id.tvNext:
                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
                int nextWorkDay = 0;
                boolean workDayFound = false;
                for (int j = dayOfWeek + 1; j < 7; j++) {
                    if (workDays.getWorkDays().get(j)) {
                        workDayFound = true;
                        break;
                    }
                    nextWorkDay++;
                }
                if (!workDayFound) {
                    for (int j = 0; j <= dayOfWeek; j++) {
                        if (workDays.getWorkDays().get(j)) {
                            break;
                        }
                        nextWorkDay++;
                    }
                }
                calendar.add(Calendar.DATE, nextWorkDay + 1);
                getScheduleData();
                break;

            case R.id.btnSchedule:
                Calendar today = Calendar.getInstance();
                try {
                    today.setTime(requestDateFormat.parse(requestDateFormat.format(today.getTime())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (calendar.before(today)) {
                    customPopup.createToast("Can't schedule meeting for past", this);
                } else {
                    Intent intent = new Intent(this, ScheduleMeetingActivity.class);
                    intent.putExtra(KEY_SCHEDULE, scheduleData);
                    startActivity(intent);
                }
                break;

            case R.id.btnClosePopup:
                dialog.dismiss();
                break;

            case R.id.btnStartTime:
                ifStartTime = true;
                Calendar startTimeCal = Calendar.getInstance();
                try {
                    startTimeCal.setTime(workTimeFormat.parse(workTime.getWorkStartTime()));
                    TimePickerDialog timePickerDialog = new TimePickerDialog(this, this, startTimeCal.get(Calendar.HOUR_OF_DAY),
                            startTimeCal.get(Calendar.MINUTE), false);
                    timePickerDialog.show();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                break;

            case R.id.btnEndTime:
                ifStartTime = false;
                Calendar endTimeCal = Calendar.getInstance();
                try {
                    endTimeCal.setTime(workTimeFormat.parse(workTime.getWorkEndTime()));
                    TimePickerDialog timePickerDialog = new TimePickerDialog(this, this, endTimeCal.get(Calendar.HOUR_OF_DAY),
                            endTimeCal.get(Calendar.MINUTE), false);
                    timePickerDialog.show();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    @Override
    public void onRefresh() {
        getScheduleData();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        calendar.set(Calendar.DATE, day);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        getScheduleData();
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
        Calendar startTimeCal = Calendar.getInstance();
        Calendar endTimeCal = Calendar.getInstance();

        Calendar calS = Calendar.getInstance();
        Calendar calE = Calendar.getInstance();
        String startTime = workTime.getWorkStartTime();
        String endTime = workTime.getWorkEndTime();
        try {
            calS.setTime(workTimeFormat.parse(startTime));
            calE.setTime(workTimeFormat.parse(endTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        startTimeCal.set(Calendar.HOUR_OF_DAY, calS.get(Calendar.HOUR_OF_DAY));
        endTimeCal.set(Calendar.HOUR_OF_DAY, calE.get(Calendar.HOUR_OF_DAY));
        startTimeCal.set(Calendar.MINUTE, calS.get(Calendar.MINUTE));
        endTimeCal.set(Calendar.MINUTE, calE.get(Calendar.MINUTE));
        Calendar temp = Calendar.getInstance();
        temp.set(Calendar.HOUR_OF_DAY, hour);
        temp.set(Calendar.MINUTE, minute);
        if (ifStartTime) {
            if (temp.after(endTimeCal)) {
                customPopup.createToast("Start time should be before end time", this);
                return;
            } else {
                workTime.setWorkStartTime(workTimeFormat.format(temp.getTime()));
            }
        } else {
            if (temp.before(startTimeCal)) {
                customPopup.createToast("End time should be after the start time", this);
                return;
            } else {
                workTime.setWorkEndTime(workTimeFormat.format(temp.getTime()));
            }
        }
        sharedPrefUtil.addWorkTimeDataToPref(workTime);
        tvStart.setText(workTime.getWorkStartTime());
        tvEnd.setText(workTime.getWorkEndTime());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        containerLayout.removeAllViews();
        switch (item.getItemId()) {
            case R.id.menuWorkday:
                tvTitle.setText(getString(R.string.title_workDay));
                workDays = sharedPrefUtil.getWorkDayDataFromPref();
                for (int i = 0; i < 7; i++) {
                    View view = layoutInflater.inflate(R.layout.item_workday, null, false);
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBoxWorkDay);
                    checkBox.setChecked(workDays.getWorkDays().get(i));
                    checkBox.setText(workdaysArray[i]);
                    checkBox.setOnCheckedChangeListener(new CheckBoxManager(i));
                    containerLayout.addView(view);
                }
                break;
            case R.id.menuWorkTime: {
                workTime = sharedPrefUtil.getWorkTimeDataFromPref();
                tvTitle.setText(getString(R.string.title_workTime));
                View view = layoutInflater.inflate(R.layout.item_worktime, null, false);
                tvStart = (TextView) view.findViewById(R.id.btnStartTime);
                tvEnd = (TextView) view.findViewById(R.id.btnEndTime);
                tvStart.setOnClickListener(this);
                tvEnd.setOnClickListener(this);
                tvStart.setText(workTime.getWorkStartTime());
                tvEnd.setText(workTime.getWorkEndTime());
                containerLayout.addView(view);
            }
            break;
            case R.id.menuSlot: {
                slotTime = sharedPrefUtil.getSlotDataFromPref();
                tvTitle.setText(getString(R.string.title_slot));
                View view = layoutInflater.inflate(R.layout.item_slot, null, false);
                SwitchCompat switchCompat = (SwitchCompat) view.findViewById(R.id.toggleSwitch);
                switchCompat.setOnCheckedChangeListener(this);
                if (slotTime == 0) {
                    switchCompat.setChecked(false);
                } else {
                    switchCompat.setChecked(true);
                }
                containerLayout.addView(view);
            }
            break;
        }
        dialog.show();
        return true;
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            sharedPrefUtil.addSlotDataToPref(1);
        } else {
            sharedPrefUtil.addSlotDataToPref(0);
        }
    }

    class CheckBoxManager implements OnCheckedChangeListener {

        int position;

        public CheckBoxManager(int position) {
            this.position = position;
        }


        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (b) {
                workDays.getWorkDays().set(position, b);
            } else {
                int j = 0;
                int trueCount = 0;
                for (Boolean bool : workDays.getWorkDays()) {
                    if (bool) {
                        trueCount++;
                    }
                }
                if (trueCount == 1) {
                    customPopup.createToast("You must select at least 1 work day", HomeActivity.this);
                    compoundButton.setChecked(true);
                } else {
                    workDays.getWorkDays().set(position, b);
                }
            }
            sharedPrefUtil.addWorkDayDataToPref(workDays);
        }
    }

}
