package com.assignment.apiResponse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Saurabh on 30-06-2016.
 */
public class Schedule implements Serializable {
    @SerializedName("start_time")
    public String startTime;
    @SerializedName("end_time")
    public String endTime;
    @SerializedName("description")
    public String description;
    @SerializedName("participants")
    public List<String> participants;
}
