package com.assignment.util;


import com.assignment.apiResponse.Schedule;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface WebServices {

    @GET("/schedule")
    void getSchedule(@Query("date") String date, Callback<List<Schedule>> callback);
}
