package com.assignment.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.assignment.entities.ScheduleData;
import com.assignment.entities.WorkDays;
import com.assignment.entities.WorkTime;
import com.google.gson.Gson;

/**
 * Created by Saurabh on 01-07-2016.
 */
public class SharedPrefUtil implements Constants{
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Gson gson;
    public SharedPrefUtil(Context context) {
        sharedPreferences =context.getSharedPreferences(MY_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        gson = new Gson();
    }


    public void addScheduleDataToPref(ScheduleData scheduleData) {
        String data = gson.toJson(scheduleData);
        editor.putString(KEY_SCHEDULE, data);
        editor.apply();
    }

    public ScheduleData getScheduleDataFromPref() {
        return gson.fromJson(sharedPreferences.getString(KEY_SCHEDULE, ""), ScheduleData.class);
    }

    public void addWorkDayDataToPref(WorkDays workDays){
        String data = gson.toJson(workDays);
        editor.putString(KEY_WORK_DAYS, data);
        editor.apply();
    }

    public WorkDays getWorkDayDataFromPref(){
        return gson.fromJson(sharedPreferences.getString(KEY_WORK_DAYS, ""), WorkDays.class);
    }

    public void addWorkTimeDataToPref(WorkTime workTime){
        String data = gson.toJson(workTime);
        editor.putString(KEY_WORK_TIME, data);
        editor.apply();
    }

    public WorkTime getWorkTimeDataFromPref(){
        return gson.fromJson(sharedPreferences.getString(KEY_WORK_TIME, ""), WorkTime.class);
    }

    public void addSlotDataToPref(int slot){
        editor.putInt(KEY_TIME_SLOT, slot);
        editor.apply();
    }

    public Integer getSlotDataFromPref(){
        return sharedPreferences.getInt(KEY_TIME_SLOT, 0);
    }
}
